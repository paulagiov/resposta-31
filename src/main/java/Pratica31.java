import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paula Giovanna Rodrigues
 */


public class Pratica31 {
    private static Date inicio = new Date();
    private static String meuNome = "pAuLa giovaNna Rodrigues";
    private static String nomeFormat;
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1999, 7, 15);
    private static GregorianCalendar hoje = new GregorianCalendar();
    
    
    public static void main(String[] args) {
        System.out.println( meuNome.toUpperCase());
        nomeFormat = meuNome.toUpperCase().charAt(15) + meuNome.toLowerCase().substring(16, 24) + ", " + meuNome.toUpperCase().charAt(0) + ". " + meuNome.toUpperCase().charAt(6) + ".";
        System.out.println(nomeFormat);
        
        long dias = (hoje.getTime().getTime() - dataNascimento.getTime().getTime())/86400000;
        System.out.println(dias);
        
        Date fim = new Date();
        System.out.println(fim.getTime() - inicio.getTime());
    }
}